FROM node:alpine
WORKDIR /usr/app
COPY package.json ./
COPY yarn.lock ./
COPY ./ ./
RUN yarn install
EXPOSE 3000
CMD ["yarn", "run", "start"]