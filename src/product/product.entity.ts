import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  description: string;

  @Column()
  amountInStock: number;

  constructor(product?: Partial<Product>) {
    this.id = product?.id;
    this.name = product?.name;
    this.price = product?.price;
    this.description = product?.description;
    this.amountInStock = product?.amountInStock;
  }

}
