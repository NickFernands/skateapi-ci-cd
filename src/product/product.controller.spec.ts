import { Test, TestingModule } from '@nestjs/testing'
import { ProductService } from './product.service'
import { ProductController } from './product.controller'
import { Product } from './product.entity'

const productsList: Product[] = [
    new Product({id: "1" , name: "Shape Brabo", price: 150.00 , description: "Shape brabao anti quedas", amountInStock: 30}),
    new Product({id: "2" , name: "Rodas muito rápidas", price: 40.00 , description: "Kit com 4 rodas muito loucas", amountInStock: 50}),
]

describe('ProductController', () => {
    let productController: ProductController
    let productService: ProductService

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ProductController],
            providers: [
                {
                    provide: ProductService,
                    useValue: {
                        findAll: jest.fn().mockResolvedValue(productsList),
                        add: jest.fn(),
                        delete: jest.fn(),
                        findById: jest.fn(),
                        update: jest.fn(),
                    }
                }
            ]
        }).compile()

        productController = module.get<ProductController>(ProductController)
        productService = module.get<ProductService>(ProductService)
    })


    it('should be defined', () => {
        expect(productController).toBeDefined()
        expect(productService).toBeDefined()
    })

    describe('findAll', () => {
        it('should return all products', async () => {
            const result = await productController.findAll()

            expect(result).toEqual(productsList)
            expect(productService.findAll).toHaveBeenCalledTimes(1)
        })

        it('should throw an exception', () => {
            jest.spyOn(productService, 'findAll').mockRejectedValueOnce(new Error())
            expect(productService.findAll).rejects.toThrowError()
        })
    })

})

